//casperjs --ignore-ssl-errors=true --pre=config.js test test002.js
/*
**Test para probar el cambio de centro desde el super con nuevo usuario.
*/
var require = patchRequire(require);
const login   = require('actions/login');
const cart   = require('actions/cart');
const xpath   = require('casper').selectXPath;
var f_s = require('../selectors/food').selectors;
var mya_s = require('../selectors/my_account').selectors;

var countFunction = function(count) {
  moquete.thenClickAndWaitForSelector (f_s.plp.add_no_in_cart_s, '('+ f_s.plp.in_cart_s +')['+ count +']',
    function success() {
      this.test.assertEquals(this.getElementsInfo(xpath(f_s.plp.in_cart_s)).length,count, 'Hay '+ count +' elementos en el carro');
    },
    function fail() {
      this.echo(this.getElementsInfo(xpath(f_s.plp.in_cart_s)).length);
      this.echo('Fallo al añadir el producto');
  });
};


moquete.test.begin('Test002', function(test) {

  moquete._start();
     cart.delete(endpoints.nft.cart,users.test002.cuc,'01',
   function then() {
    this.test.assertHttpStatus(204);
   },
  function timeout() {
    this.echo('Fallo al hacer el delete');
  });

  moquete.thenOpenAndWaitForSelector(base_url, f_s.modals.cookies,
      function success(){
        this.test.assertExists(xpath(f_s.modals.cookies), 'Existe la modal de cookies');
      },
      function fail(){
        this.echo("No existe el campo para buscar")
   });
    moquete.thenClickAndWaitForSelector(f_s.modals.cookies, f_s.searching.input,
      function success(){
        this.test.assertExists(xpath(f_s.searching.input), 'Existe el campo para la búsqueda');
        //this.thenClick(xpath('//*[@id="cookies_agree"]'));
        this.sendKeys("input[name='term']", "platanos"); 
      },
      function fail(){
        this.echo("No existe el campo para buscar")
   });
   moquete.thenWaitForSelector (f_s.searching.panel_products, 
        function success() {
            this.test.assertExists(xpath(f_s.searching.units_up), 'Existe el botón de aumentar unidades');
            this.test.assertExists(xpath(f_s.searching.units_down), 'Existe el botón de decrementar unidades');
        },
        function fail() {
            this.echo('No hay resultados en el typeahead');
        }
    );

   moquete.thenClickAndWaitForSelector (f_s.searching.units_up, f_s.modals.login.sign_in,
        function success() {
            this.test.assertExists(xpath(f_s.modals.login.sign_in), 'Existe el botón de iniciar sesión');
            this.test.assertExists(xpath(f_s.modals.login.sign_up), 'Existe el botón de iniciar sesión');
        },
        function fail() {
            this.echo('No existe modal para iniciar sesión');
        }
    );
   moquete.thenClickAndWaitForUrl(f_s.modals.login.sign_in, /oauth\/authorize/,
        function () {
            this.test.assertExists(xpath(mya_s.login.submit), 'Existe el botón de Acceder');
        },
        function () {
            this.echo('Fallo al clickar en iniciar sesión', 'ERROR'); 
        });
   
    login.food_login(users.test002.emailLogin,users.test002.passwordLogin, 
      function callback(){
         this.test.assertExists(xpath(f_s.login.logged), 'Se ha logado el usuario');
      });

     moquete._waitForSelector (f_s.searching.input, 
        function success() {
          this.test.assertExists(xpath(f_s.searching.input));
          this.sendKeys("input[name='term']", "platanos");
        },
        function fail() {
            this.echo('No existe el campo para buscar');
        });

    moquete._waitForSelector (f_s.searching.panel_products, 
        function success() {
            this.test.assertExists(xpath(f_s.searching.units_up), 'Existe el botón de aumentar unidades');
            this.test.assertExists(xpath(f_s.searching.units_down), 'Existe el botón de decrementar unidades');
        },
        function fail() {
            this.echo('No hay resultados en el typeahead');
        });

    moquete.thenClickAndWaitForSelector (f_s.searching.units_up, f_s.start_order.from_modal,
        function success() {
            this.test.assertExists(xpath(f_s.start_order.from_modal), 'Existe la modal para iniciar orden');
        },
        function fail() {
            this.echo('No existe modal para iniciar pedido');
        }
    );
  moquete.thenClickAndWaitForSelector (f_s.start_order.from_modal, f_s.select_shipping.choose_centre,
        function success() {
            this.test.assertExists(xpath(f_s.select_shipping.choose_centre), 'Existe seleccionar centro');
            this.test.assertExists(xpath(f_s.select_shipping.choose_address), 'Existe seleccionar dirección');
        },
        function fail() {
            this.echo('Fallo al ir a la página de seleccionar metodo de envio');
        }
    );
   moquete.thenClickAndWaitForSelector (f_s.select_shipping.choose_centre, f_s.select_centre.droppable,
        function success() {
            this.test.assertExists(xpath(f_s.select_centre.droppable), 'Existe el desplegable de centros');
        },
        function fail() {
            this.echo('Fallo al ir a elegir centro');
    });
    moquete.thenClickAndWaitForSelector (f_s.select_centre.droppable, f_s.select_centre.barcelona,
        function success() {
            this.test.assertExists(xpath(f_s.select_centre.barcelona), 'Existe BCN');
        },
        function fail() {
            this.echo('Fallo al clickar el desplegable');
    });

     moquete.thenClickAndWaitForSelector (f_s.select_centre.barcelona, f_s.select_centre.plaza_catalunya,
        function success() {
            this.test.assertExists(xpath(f_s.select_centre.plaza_catalunya), 'Existe El centro Plaza de Catalunya' );
        },
        function fail() {
            this.echo('Fallo al clickar en BCN');
    });
     moquete.thenClickAndWaitForSelector (f_s.select_centre.plaza_catalunya, f_s.modals.catalonia.close,
        function success() {
            this.test.assertExists(xpath(f_s.modals.catalonia.close), 'Existe la modal de site en catalan' );
        },
        function fail() {
            this.echo('Fallo al seleccionar plaza de cataluña');
    });
     moquete.thenClickAndWaitWhileSelector (f_s.modals.catalonia.close, f_s.modals.catalonia.close,
        function success() {           
            if(this.exists(xpath(f_s.start_order.from_new_user_modal))){
              this.thenClickAndWaitWhileSelector(f_s.start_order.from_new_user_modal, f_s.start_order.from_new_user_modal) 
            }else{
              this.test.assertDoesntExist(xpath(f_s.modals.catalonia.close), 'Se ha cerrado la modal de catalan');
              this.test.assertExists(xpath(f_s.user_controls.panel), 'Existe el panel de usuario');
              this.test.assertExists(xpath(f_s.user_controls.my_orders), 'Existe mis pedidos');
              this.test.assertExists(xpath(f_s.user_controls.my_lists), 'Existe mis listas');
              this.test.assertExists(xpath(f_s.user_controls.quick_list), 'Existe lista rápida');
              this.test.assertExists(xpath(f_s.user_controls.my_usuals), 'Existe mis habituales');
            }
        },
        function fail() {
            this.echo('Fallo al cerrar la modal de catalán');
    });
    moquete.thenClickAndWaitForSelector (f_s.user_controls.quick_list, f_s.quick_list.modal_new_user,
      function success() {
          this.test.assertExists(xpath(f_s.quick_list.modal_new_user), 'Existe la modal de ayuda de lista rapida');
      },
      function fail() {
        this.echo('Fallo al ir a lista rápida');
    });
    moquete.thenClickAndWaitForSelector (f_s.quick_list.modal_new_user, f_s.quick_list.term,
      function success() {
        if(this.exists(xpath(f_s.quick_list.delete))){
          this.thenClickAndWaitWhileVisible(f_s.quick_list.delete, f_s.quick_list.delete) 
        }else{
          this.test.assertDoesntExist(xpath(f_s.quick_list.delete), 'La lista está vacía' );
        } 
      },
      function fail() {
        this.echo('Fallo al ir a lista rápida');
    });
    moquete.thenWaitForSelector (f_s.quick_list.term, 
        function success() {
          this.test.assertExists(xpath(f_s.quick_list.term));
          this.sendKeys("input.form-input._big._custom_placeholder.js-shopping-list-input", "leche el corte ingles");
          //this.sendKeys("input.form-input._big._custom_placeholder.js-shopping-list-input", "pollo");
          //this.sendKeys("input.form-input._big._custom_placeholder.js-shopping-list-input", "pepsi max");

        },
        function fail() {
            this.echo('No existe el campo para buscar');
    });
     moquete.thenClickAndWaitForSelector (f_s.quick_list.add, f_s.quick_list.list, 
        function success() {
            this.test.assertEquals(this.getElementsInfo(xpath(f_s.quick_list.list)).length,1, 'Hay un elemento en la lista');
            this.test.assertExists(xpath(f_s.quick_list.start), 'Existe el botón de comenzar');
        },
        function fail() {
           this.echo('Fallo en la lista rápida');
    });
   
    moquete.thenWaitFor(f_s.quick_list.list, 1,
      function then() {
      this.sendKeys("input.form-input._big._custom_placeholder.js-shopping-list-input", "pollo");
     },
        function fail() {
           this.echo('Fallo en la lista rápida');
    });
      moquete.thenClickAndWaitForSelector (f_s.quick_list.add, f_s.quick_list.list, 
        function success() {
            this.test.assertEquals(this.getElementsInfo(xpath(f_s.quick_list.list)).length,2, 'Hay 2 elementos en la lista');
            this.test.assertExists(xpath(f_s.quick_list.start), 'Existe el botón de comenzar');
        },
        function fail() {
           this.echo('Fallo en la lista rápida');
    });
    moquete.thenWaitFor(f_s.quick_list.list, 2,
      function then() {
      this.sendKeys("input.form-input._big._custom_placeholder.js-shopping-list-input", "san miguel rubia 12 33cl");
     },
        function fail() {
           this.echo('Fallo en la lista rápida');
    });
      moquete.thenClickAndWaitForSelector (f_s.quick_list.add, f_s.quick_list.list, 
        function success() {
            this.test.assertEquals(this.getElementsInfo(xpath(f_s.quick_list.list)).length,3, 'Hay 3 elementos en la lista');
            this.test.assertExists(xpath(f_s.quick_list.start), 'Existe el botón de comenzar');
        },
        function fail() {
           this.echo('Fallo en la lista rápida');
    });
    moquete.thenClickAndWaitForSelector (f_s.quick_list.start, f_s.quick_list.home_panel,
        function success() {
          this.test.assertExists(xpath(f_s.quick_list.home_panel), 'Existe el panel de lista rápida');
          this.test.assertExists(xpath(f_s.quick_list.current_term), 'Existe el término buscado');
          //this.test.assertExists(xpath(f_s.quick_list.prev_term), 'Existe el término anterior');
          //this.test.assertExists(xpath(f_s.quick_list.next_term), 'Existe el término posterior');
        },
        function fail() {
            this.echo('Fallo al iniciar lista rápida');
    });

    moquete.then(function(){
      for (var count=1; count<5;count++) countFunction(count);
      moquete.thenClickAndWaitForSelector (f_s.quick_list.next_term, f_s.plp.add_no_in_cart_s,
          function success() {
            this.test.assertExists(xpath(f_s.plp.add_no_in_cart_s), 'No existe productos añadidos');
          },
          function fail() {
              this.echo('Fallo al buscar pollo');
      });
    });

    moquete.then(function(){ 
    for (var count=1; count<4;count++) countFunction(count);
   moquete.thenClickAndWaitForSelector (f_s.quick_list.next_term, f_s.plp.add_no_in_cart_s,
        function success() {
          this.test.assertExists(xpath(f_s.plp.add_no_in_cart_s), 'No existe productos añadidos');
        },
        function fail() {
            this.echo('Fallo al buscar la cerveza');
      });
    }); 
     moquete.thenClickAndWaitForSelector (f_s.plp.add_no_in_cart_s,  f_s.plp.in_cart_s,
        function success() {
          this.test.assertEquals(this.getElementsInfo(xpath(f_s.plp.in_cart_s)).length,1, 'Hay 1 elementos en el carro');
        },
        function fail() {
            this.echo('Fallo al añadir al carro la cerveza');
    });
    moquete.thenClickAndWaitForSelector (f_s.user_controls.delivery_place,  f_s.select_shipping.choose_centre,
        function success() {
          this.test.assertExists(xpath(f_s.select_shipping.choose_centre), 'No existe seleccionar centro');
        },
        function fail() {
            this.echo('Fallo al pinchar opciones de envío');
    });
    moquete.thenClickAndWaitForSelector (f_s.select_shipping.choose_centre, f_s.select_centre.droppable,
        function success() {
            this.test.assertExists(xpath(f_s.select_centre.droppable), 'Existe el desplegable de centros');
        },
        function fail() {
            this.echo('Fallo al ir a elegir centro');
    });
    moquete.thenClickAndWaitForSelector (f_s.select_centre.droppable, f_s.select_centre.madrid,
        function success() {
            this.test.assertExists(xpath(f_s.select_centre.madrid), 'Existe Madrid');
        },
        function fail() {
            this.echo('Fallo al clickar el desplegable');
    });

     moquete.thenClickAndWaitForSelector (f_s.select_centre.madrid, f_s.select_centre.gasolinera,
        function success() {
            this.test.assertExists(xpath(f_s.select_centre.gasolinera), 'Existe El centro CTC Coslada' );
        },
        function fail() {
            this.echo('Fallo al clickar en Madrid');
    });
    moquete.thenClickAndWaitForSelector (f_s.select_centre.gasolinera, f_s.modals.change_center.accept,
        function success() {
            this.test.assertExists(xpath(f_s.modals.change_center.accept), 'Existe la modal de cambio en las opciones de carro');
            this.test.assertExists(xpath(f_s.modals.change_center.cancel), 'Existen el botón del cancelar');
            this.test.assertExists(xpath(f_s.modals.change_center.close), 'Existen el botón de cerrar');
        },
        function fail() {
            this.echo('Fallo al seleccionar coslada');
    });
     moquete.thenClickAndWaitForSelector (f_s.modals.change_center.accept, f_s.modals.product_adjusted.accept,
        function success() {
            this.test.assertExists(xpath(f_s.modals.product_adjusted.accept), 'Existe la modal de ajuste de productos');
            this.test.assertExists(xpath(f_s.modals.product_adjusted.items), 'Existen productos ajustados');
            this.test.assertExists(xpath(f_s.modals.product_adjusted.quantity_ajusted), 'Existen productos ajustados');
        },
        function fail() {
            this.echo('Fallo al cambiar el pedido');
    });
      moquete.thenClickAndWaitForSelector(f_s.modals.product_adjusted.accept, f_s.mini_cart.show,
        function success() {
            this.test.assertExist(xpath(f_s.mini_cart.show), 'Existe el minicarro');
        },
        function fail() {
            this.echo('Fallo al cerrar la modal de ajuste');
    });
    moquete.thenClickAndWaitForSelector(f_s.mini_cart.show, f_s.mini_cart.full_cart,
        function success() {
            this.test.assertExist(xpath(f_s.mini_cart.full_cart), 'Existe el botón ir a la página de carro');
        },
        function fail() {
            this.echo('Fallo al abrir el minicarro');
    });
    moquete.thenClickAndWaitForSelector(f_s.mini_cart.full_cart, f_s.cart.to_checkout,
        function success() {
            this.test.assertExist(xpath(f_s.mini_cart.full_cart), 'Existe el botón ir a la página de carro');
            this.test.assertExist(xpath(f_s.cart.delete_cart), 'Existe el botón borrar el carro');
            this.test.assertExist(xpath(f_s.cart.delete_item), 'Existe el botón para borrar items');
            this.test.assertExist(xpath(f_s.cart.save_as_list), 'Existe el botón para guardar una lista');
        },
        function fail() {
            this.echo('Fallo al ir a la página de carro');
    });
    /*** Borrado de elementos del carro**
    moquete.thenClickAndWaitForSelector(f_s.cart.delete_item, f_s.cart.notification,
        function success() {
            this.test.assertEquals(this.getElementsInfo(xpath(f_s.cart.item)).length,6, 'Hay un elemento en la lista');
        },
        function fail() {
            this.echo('Fallo al borrar elemento');
    });
     moquete.thenClickAndWaitForSelector(f_s.cart.delete_item, f_s.cart.notification,
        function success() {
            this.test.assertEquals(this.getElementsInfo(xpath(f_s.cart.item)).length,5, 'Hay un elemento en la lista');
        },
        function fail() {
            this.echo('Fallo al borrar elemento');
    });*/
       moquete.thenClickAndWaitForSelector(f_s.cart.to_checkout, f_s.checkout.to_pgp,
        function success() {
          if(this.exists(xpath(f_s.checkout.to_pgp))){
            this.test.assertExist(xpath(f_s.checkout.to_pgp), 'Existe el botón para ir a la PGP');
         }else{
            this.thenClickAndWaitWhileSelector(f_s.loyalty.to_checkout, f_s.checkout.to_pgp)
         }
        },
        function fail() {
            this.echo('Fallo al ir al checkout');
    });
    moquete.thenClickAndWaitForSelector(f_s.checkout.to_pgp, f_s.modals.min_cAc.text,
        function success() {
            this.test.assertExist(xpath(f_s.checkout.to_pgp), 'Existe el botón para ir a la PGP');
        },
        function fail() {
            this.echo('Fallo al ir la PGP');
    });

   moquete.run(function() {test.done(); this.exit()});
});