//casperjs --ignore-ssl-errors=true --pre=config.js test e2e.js

const moquete = require('moquete');
const login   = require('actions/login');

casper.test.begin('shopping', function suite() {
    /*
    casper.start(function () {
        this.open('base_url');
        this.test.assertHttpStatus(200);
        this.test.comment('Open supermarket tab');
    });*/
    moquete.start('base_url', '[moquete] Open Supermarket Tab');
    /*
    casper.thenOpen(base_url, function(){
        this.test.assertHttpStatus(200);
        this.test.comment('Open the page ('+ this.getTitle() + ')');
        this.test.info('Current location is ' + this.getCurrentUrl());
        this.test.assertExist({type: 'xpath', path: '//nav[@class="top_menu c12"]' }, 'Tab bar');
    });
    */
    moquete.open(base_url, '//span[text()="Alimentación"]');
    /*
    casper.thenClick({type: 'xpath', path: '//span[text()="Alimentación"]' }, function(){
        this.waitForUrl(base_url + 'alimentacion-general');
        this.test.info('Current location is ' + this.getCurrentUrl());
        this.test.assertHttpStatus(200);
        this.test.assertExist({type: 'xpath', path: '//ul[@class="tree-node"]' }, 'arbol de navegacion');
        this.click({type: 'xpath', path: '//a[text()="Arroz"]'});
        this.waitFor(function check () {
            return (this.getCurrentUrl()=== base_url + 'alimentacion-general/arroz/');
        },
        function then (){
            this.test.assertExist({type: 'xpath', path: '//button[@class="product_controls-button _buy button _primary _not_perishable js-add-item js-tooltip"]' }, 'existen productos para añadir');

        },
        function timeout() { // step to execute if check has failed
        this.echo('FAllo al ir a arroz', 'ERROR');
        });

    });
    */
    moquete.thenClickAndWaitForUrl('//span[text()="Alimentación"]', base_url + 'alimentacion-general', function() {
        this.test.info('Current location is ' + this.getCurrentUrl());
        this.test.assertHttpStatus(200);
        this.test.assertExist({type: 'xpath', path: '//ul[@class="tree-node"]' }, 'arbol de navegacion');
    });

    moquete.thenClickAndWaitForUrl('//a[text()="Arroz"]', base_url + 'alimentacion-general/arroz/',
        function() {
            this.test.assertExist({type: 'xpath', path: '//button[@class="product_controls-button _buy button _primary _not_perishable js-add-item js-tooltip"]' }, 'existen productos para añadir');
        },
        function() {
            this.echo('FAllo al ir a arroz', 'ERROR');
        }
    );
    casper.then( function(){
        this.clickLabel('añadir al carro', 'span');
        this.wait(4000);

    });
    login.food_modal_login();
    /*
  casper.waitForSelector({type: 'xpath', path: '//a[text()="Iniciar sesión"]'},
       function success() {
           this.test.assertExists({type: 'xpath', path: '//*[@id="modal-content"]/div/div/div[3]/a[1]'},'Existe la modal para iniciar sesión');
           this.test.assertExists({type: 'xpath', path: '//a[text()="Iniciar sesión"]'}, 'Existe el botón de iniciar sesión');
           this.click({type: 'xpath', path: '//*[@id="modal-content"]/div/div/div[3]/a[1]'}); 
           this.wait(4000);
       },
       function fail() {
          this.echo('Fallo al clickar en iniciar sesión', 'ERROR'); 
  });
    casper.then(function(){
        this.test.comment('Fill and submit login form');
        this.fillXPath('form[class="js-checkeable-form c12"]', {
            '//input[@name="login"]':    emailLogin,
            '//input[@name="password"]':    passwordLogin,
    }, true);
        this.test.assertExists({type: 'xpath', path: '//*[@type="submit"]'}, 'Existe el botón de Acceder');
        this.click({type: 'xpath', path:'//*[@type="submit"]'}); 
        this.wait(1000);
    });
    */

    casper.thenClick({type: 'xpath', path: '//div[@class="minicart-header js-show-minicart"]' }, function(){
        this.test.assertExist({type: 'xpath', path: '//a[@class="button _primary minicart-buy_cart js-go-to-full-cart"]' }, 'Existe el botón al carro');
        this.test.pass('Desplegando la cesta flotante');
    });
    casper.thenClick({type: 'xpath', path: '//a[@class="button _primary minicart-buy_cart js-go-to-full-cart"]'}, function(){
        this.test.info('Current location is ' + this.getCurrentUrl());
        this.test.assertHttpStatus(200);
        this.waitFor(function check() {
            return (this.getCurrentUrl()=== base_url + 'alimentacion/carro/');
        },
        function then (){
            this.test.assertExist({type: 'xpath', path: '//a[@class="button _primary js-process-cart]' }, 'Existe el botón tramitar');
        },
        function timeout() { // step to execute if check has failed
        this.echo('FAllo al ir al carro', 'ERROR');
        });
    });
    casper.thenClick({type: 'xpath', path: '//a[@class="button _primary js-process-cart"]'}, function(){
        this.test.assertExist({type: 'xpath', path: '//button[@class="btn submit js-submit-shipping"]' }, 'Existe el botón de ir a PGP');
        this.test.assertExist({type: 'xpath', path: '//*[@id="modalSlot"]/div/div/div/div[2]/div[2]/div/table/tbody/tr[2]/td[2]' }, 'Existen slots abiertos');
    });
    casper.thenClick({type: 'xpath', path: '//input[@class="form-radio _slot js-slot-input"][contains(@value,"OPEN")]'}, function(){
        this.test.assertExist({type: 'xpath', path: '//*[@id="js-clock-container"]/span[3]/span' }, 'Existe el contador del tiempo');
        this.test.assertExist({type: 'xpath', path: '//*[@id="js-slot-info"]/p' }, 'Existe la info del tramo reservado');
        this.wait(1000);
    });
    casper.thenClick({type: 'xpath', path: '//*[@id="unfolded-cart-summary-container"]/div[4]/button'}, function () {
        this.wait(10000);
        this.test.assertExist({type: 'xpath', path: '//*[@id="RECEIVABLE"]/a/span[2]/span' }, 'Existe pago contrareembolso');
    });
    casper.thenClick({type: 'xpath', path: '//*[@id="RECEIVABLE"]/a/span[2]/span'}, function () {
        this.wait(10000);
        this.test.assertExist({type: 'xpath', path: '//*[@id="PAGAR_COMPRA"]' }, 'Existe el botón de pagar');
    });
    /*
    casper.thenClick({type: 'xpath', path: '//*[@id="PAGAR_COMPRA"]'}, function(){ 
        this.wait(10000);
    });*/

//*[@id="modalSlot"]/div/div/div/div[2]/div[2]/div/table/tbody/tr[2]/td[5]/label
    /*casper.thenClick('div.add-to-cart-buttons button.btn-cart', function(){
        this.waitForUrl(base_url + 'checkout/cart/');
        this.test.info('Current location is ' + this.getCurrentUrl());
        this.test.assertHttpStatus(200);
        this.test.assertSelectorHasText('ul.messages li.success-msg', prod_name_simple +' was added to your shopping cart.', 'Success msg - is present');
        this.test.assertElementCount('#shopping-cart-table tbody tr', 1, '1 expected products have found ');
        this.test.assertExist('div.page  div.cart-totals-wrapper button.button.btn-proceed-checkout.btn-checkout', 'Checkout button - is present');
        this.test.assertExist('#discount-coupon-form', 'Discount coupon form - is present');
        this.test.assertExist('div.cart-forms div.shipping', 'ESTIMATE SHIPPING AND TAX form - is present');
        this.test.assertExist('#shopping-cart-totals-table', 'Totals table is present');
        this.test.pass('Shopping cart opened successfully')
    });

    casper.thenOpen(base_url + prod_url_conf, function(){
        this.test.assertHttpStatus(200);
        this.test.comment('Open configurable product page ('+ prod_url_conf + ')');
        this.test.info('Current location is ' + this.getCurrentUrl());
        this.test.assertSelectorExist('div.add-to-cart-buttons button.btn-cart', 'Add to cart btn - is present');
        this.test.assertSelectorHasText('div.product-shop div.product-name span.h1', prod_name_conf, 'Product name - is present');
        this.test.assertSelectorExist('div.product-shop div.price-box span.price', 'Product price - is present');
        this.test.assertSelectorExist('#qty', 'Qty input field - is present');
        this.test.comment('Set options and add product to the cart');
        this.test.assertExist('#product-options-wrapper', 'Product options - is present');
        //              Click on white color swatch
        this.test.assertExist('#configurable_swatch_color', 'Color swatch - is present');
        this.click('#configurable_swatch_color li.option-white a span.swatch-label');
        this.test.assertSelectorExist('#configurable_swatch_color li.option-white.selected', 'Color swatch has been selected');
        //              Click on L size
        this.test.assertExist('#configurable_swatch_size', 'Side swatch - is present');
        this.test.assertSelectorExist('#configurable_swatch_size li.option-s.not-available', 'S size is not available for the white color swatch');
        this.click('#configurable_swatch_size li.option-l a span.swatch-label');
        this.test.assertSelectorExist('#configurable_swatch_size li.option-l.selected');
        //fill monogram field
        this.sendKeys('#options_3_text', 'Atwix test');
        var test_msg = this.evaluate(function() {
            return jQuery("#options_3_text").val();
        });

        this.test.assertEqual(test_msg, "Atwix test", "Found expected text within the textarea");

        //select test custom option
        this.evaluate(function(){
            //jQuery('#select_2').val(1);
            document.querySelector('#select_2').selectedValue = 1; //Value - 1 = model 1 +$59.00
        });
    });
    casper.thenClick('div.add-to-cart-buttons button.btn-cart', function(){
        this.waitForUrl(base_url + 'checkout/cart/');
        this.test.info('Current location is ' + this.getCurrentUrl());
        this.test.assertHttpStatus(200);
        this.test.assertSelectorHasText('ul.messages li.success-msg', prod_name_conf +' was added to your shopping cart.', 'Success msg - is present');
        this.test.assertElementCount('#shopping-cart-table tbody tr', 2, '2 expected products have found ');
        this.test.assertExist('div.page  div.cart-totals-wrapper button.button.btn-proceed-checkout.btn-checkout', 'Checkout button - is present');
        this.test.assertExist('#discount-coupon-form', 'Discount coupon form - is present');
        this.test.assertExist('div.cart-forms div.shipping', 'ESTIMATE SHIPPING AND TAX form - is present');
        this.test.assertExist('#shopping-cart-totals-table', 'Totals table is present');
        this.test.pass('Shopping cart opened successfully')
    });
    casper.thenClick('#empty_cart_button', function(){
       this.test.comment('Click empty cart button');
        this.test.assertSelectorHasText('div.page div.page-title  h1', 'Shopping Cart is Empty', 'Shopping Cart is Empty');
        this.test.assertSelectorHasText('div.page div.cart-empty', 'You have no items in your shopping cart.', 'You have no items in your shopping cart.');
    });
*/
    casper.run(function () {
            this.test.done();
        }
    )
});
