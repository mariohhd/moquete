//casperjs --ignore-ssl-errors=true --pre=config.js test test001.js
/*
**Test para probar una compra completa con un usuario con carro iniciado.
*/
var require = patchRequire(require);
const login   = require('actions/login');
const cart   = require('actions/cart');
const xpath   = require('casper').selectXPath;
var f_s = require('../selectors/food').selectors;
var mya_s = require('../selectors/my_account').selectors;
var bodyCentre = {
    'centre_id': '010973',
    'shipping_method': {
    	'type': 'CLICKANDCAR'
    },
    'contact_info': {
    'phone_number': '666634266',
    'first_name': 'MArta',
    'last_name': 'Dor'
  }
 };
var bodyProduct = [{
	'id':'0110120912200043___',
	'quantity':42,
	'purchase_as':'SALE_TYPE_UNIT',
	'from_catalog':'supermarket'
}];
/*'id':'0110120902600244___',
	'quantity':12,
	'purchase_as':'SALE_TYPE_UNIT',
	'from_catalog':'supermarket'
},
*/
var countFunction = function(count) {
  moquete.thenClickAndWaitForSelector (f_s.plp.add_no_in_cart_s, '('+ f_s.plp.in_cart_s +')['+ count +']',
    function success() {
      this.test.assertEquals(this.getElementsInfo(xpath(f_s.plp.in_cart_s)).length,count, 'Hay '+ count +' elementos en el carro');
    },
    function fail() {
      this.echo(this.getElementsInfo(xpath(f_s.plp.in_cart_s)).length);
      this.echo(count);
      this.echo('Fallo al añadir el producto');
  });
};


moquete.test.begin('Test001', function(test) {

  moquete._start();

cart.delete(endpoints.nft.cart, users.test001.cuc,'01',
      function then() {
        this.test.assertHttpStatus(204);
      },
    function timeout() {
    this.echo('Fallo al hacer el delete');
  });
 cart.post(endpoints.nft.cart, users.test001.cuc,'01', bodyCentre,
   function then() {
    this.test.assertHttpStatus(201);
   },
  function timeout() {
    this.echo('Fallo al hacer post para cambiar el centro');
  });
 cart.post(endpoints.nft.items, users.test001.cuc,'01', bodyProduct,
   function then() {
    this.test.assertHttpStatus(201);
   },
  function timeout() {
    this.echo('Fallo al hacer post para cambiar el centro');
  });

	moquete.thenOpenAndWaitForSelector(base_url, f_s.modals.cookies,
      function success(){
        this.test.assertExists(xpath(f_s.modals.cookies), 'Existe la modal de cookies');
      },
      function fail(){
        this.echo("No existe la modal de cookies")
   });

   	moquete.thenClickAndWaitWhileVisible(f_s.modals.cookies, f_s.modals.cookies,
    	function success(){
       		this.test.assertEquals(this.getElementsInfo(xpath(f_s.supermarket_tabs.food)).length,10, 'Hay 10 tabs');
      	},
    	function fail(){
        	this.echo("No existe la modal de cookies")
   	});

  	moquete.thenClickAndWaitForUrl(f_s.supermarket_tabs.food, /alimentacion-general/,
    	function success(){
       		this.test.assertEquals(this.getElementsInfo(xpath(f_s.supermarket_tabs.food)).length,9, 'Hay 1 tab seleccionada');
       		this.test.assertExists(xpath(f_s.categories.tree), 'Existe el árbol');
      	},
    	function fail(){
        	this.echo("No existe el árbol")
  	});

   moquete.thenClickAndWaitForUrl(f_s.categories.family, /alimentacion-general\/aceites/,
	    function success(){
	    	this.test.assertExists(xpath(f_s.plp.add_no_in_cart_s), 'Hay productos para añadir');
	     	this.test.assertExists(xpath(f_s.categories.variety), 'Existen variedades');
	     },
	    function fail(){
	        this.echo("No existe la categoria")
   	});

	moquete.thenClickAndWaitForSelector (f_s.plp.add_no_in_cart_s, f_s.modals.login.sign_in,
	    function success() {
	        this.test.assertExists(xpath(f_s.modals.login.sign_in), 'Existe el botón de iniciar sesión');
	        this.test.assertExists(xpath(f_s.modals.login.sign_up), 'Existe el botón de iniciar sesión');
	    },
	    function fail() {
	        this.echo('No existe modal para iniciar sesión');
	});
	moquete.thenClickAndWaitForUrl(f_s.modals.login.sign_in, /oauth\/authorize/,
        function success() {
            this.test.assertExists(xpath(mya_s.login.submit), 'Existe el botón de Acceder');
        },
        function fail() {
            this.echo('Fallo al clickar en iniciar sesión', 'ERROR'); 
        });
    login.food_login(users.test001.emailLogin,users.test001.passwordLogin, 
      function callback(){
         this.test.assertExists(xpath(f_s.login.logged), 'Se ha logado el usuario');
      });
    moquete.then(function(){ 
    for (var count=1; count<10;count++) countFunction(count);
	});
	 moquete.thenClickAndWaitForSelector(f_s.mini_cart.show, f_s.mini_cart.full_cart,
        function success() {
            this.test.assertExist(xpath(f_s.mini_cart.full_cart), 'Existe el botón ir a la página de carro');
        },
        function fail() {
            this.echo('Fallo al abrir el minicarro');
    });
    moquete.thenClickAndWaitForSelector(f_s.mini_cart.full_cart, f_s.cart.to_checkout,
        function success() {
            this.test.assertExist(xpath(f_s.cart.to_checkout), 'Existe el botón para ir al checkout');
            this.test.assertExist(xpath(f_s.cart.delete_cart), 'Existe el botón borrar el carro');
            this.test.assertExist(xpath(f_s.cart.delete_item), 'Existe el botón para borrar items');
            this.test.assertExist(xpath(f_s.cart.save_as_list), 'Existe el botón para guardar una lista');
        },
        function fail() {
            this.echo('Fallo al ir a la página de carro');
    });
 	moquete.thenClickAndWaitForSelector(f_s.cart.to_checkout, f_s.checkout.to_pgp,
        function success() {
          if(this.exists(xpath(f_s.checkout.to_pgp))){
            this.test.assertExist(xpath(f_s.checkout.to_pgp), 'Existe el botón para ir a la PGP');
            this.echo(this.getElementsInfo(xpath(f_s.checkout.open_slot)).length);
            this.test.assertExists(xpath(f_s.checkout.slots_table), 'Existe la tabla de slots');
         }else{
            this.thenClickAndWaitWhileSelector(f_s.loyalty.to_checkout, f_s.checkout.to_pgp)
         }
        },
        function fail() {
            this.echo('Fallo al ir al checkout');
    });
    moquete.thenClickAndWaitForSelector(f_s.checkout.open_slot, f_s.checkout.clock,
        function success() {
            this.test.assertExist(xpath(f_s.checkout.clock), 'Existe el contador de tiempo');
            this.test.assertExist(xpath(f_s.checkout.slot_info), 'Existe la información del tramo seleccionado');
        },
        function fail() {
            this.echo('Fallo al seleccionar el tramo');
    });
    /*
      moquete._waitForSelector (f_s.checkout.eci_card_number, 
        function success() {
          this.test.assertExists(xpath(f_s.checkout.eci_card_number));
          this.sendKeys('#card_number', "0079052684");
          this.sendKeys('#pin_number', "018");
        },
        function fail() {
            this.echo('No existen los campos para teci');
        });
       moquete._waitForSelector (f_s.checkout.eci_card_yn, 
        function success() {
          this.test.assertExists(xpath(f_s.checkout.eci_card_yn));
          this.test.assertExists(xpath(f_s.checkout.eci_card_discounts));
        },
        function fail() {
            this.echo('No se ha introducido la teci');
        });
        */
 	moquete.thenClickAndWaitForSelector(f_s.checkout.to_pgp, f_s.modals.out_of_stock.to_pgp,
        function success() {
            this.test.assertExist(xpath(f_s.modals.out_of_stock.to_pgp), 'Existe la modal de productos agotados');
        },
        function fail() {
            this.echo('Fallo al continuar a PGP');
    });
    moquete.thenClickAndWaitForSelector(f_s.modals.out_of_stock.to_pgp, f_s.pgp.paypal,
        function success() {
            this.test.assertExist(xpath(f_s.pgp.paypal), 'Existe pago con paypal');
        },
        function fail() {
            this.echo('Fallo al ir a la PGP');
    });
  	moquete.thenClickAndWaitForSelector(f_s.pgp.paypal, f_s.pgp.pay,
        function success() {
            this.test.assertExist(xpath(f_s.pgp.pay), 'Existe el botón de pago paypal');
        },
        function fail() {
            this.echo('Fallo al clickar en el botón de paypal');
    });
    moquete.thenClickAndWaitForSelector(f_s.pgp.pay,f_s.paypal.sign_in,
        function success() {
            this.test.assertExist(xpath(f_s.pgp.pay), 'Existe el botón de pago paypal');
        },
        function fail() {
            this.echo('Fallo al clickar en el botón de paypal');
    });

  	moquete.thenClickAndWaitForUrlChange(
        function success() {
            this.test.assertExist(xpath(f_s.paypal.sign_in), 'No existe el cargando');
        },
        function fail() {
            this.echo('Fallo al clikar el botón de pagar');
    }, 50000);
    moquete.thenClickAndWaitForSelector(f_s.paypal.sign_in, f_s.paypal.login,
        function success() {
            this.test.assertExist(xpath(f_s.paypal.login), 'Existe el campo para introducir el login');
            this.sendKeys('#email', "pgpcomprador@gmail.com");
        },
        function fail() {
            this.echo('Fallo al iniciar sesión');
    });
    moquete.thenClickAndWaitForSelector(f_s.paypal.next, f_s.paypal.password,
        function success() {
            this.test.assertExist(xpath(f_s.paypal.password), 'Existe el campo para introducir el password');
            this.sendKeys('#password', "compradorpgp");
        },
        function fail() {
            this.echo('Fallo al dar al botón siguiente');
    });
    moquete.thenClickAndWaitForSelector(f_s.paypal.submit, f_s.paypal.paynow,
        function success() {
            this.test.assertExist(xpath(f_s.paypal.paynow), 'Existe el botón de submit');
        },
        function fail() {
            this.echo('Fallo al dar al botón de submit');
    });
   /* moquete.thenClickAndWaitForSelector(f_s.paypal.paynow, f_s.confirmation,
        function success() {
            this.test.assertExist(xpath(f_s.confirmation), 'Existe la página de confirmacion');
        },
        function fail() {
            this.echo('Fallo al dar al botón de pago en paypal');
    });
*/

    moquete.run(function() {test.done(); this.exit()});
});