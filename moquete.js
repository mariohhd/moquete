//API Casperjs - http://docs.casperjs.org/en/latest/modules/index.html

var require = patchRequire(require);
var casper = require('casper');
var Casper = casper.Casper;
var xpath = casper.selectXPath;
var utils  = require('utils');
var fs      = require('fs');


function Moquete() {
    Moquete.super_.apply(this, arguments);
};
utils.inherits(Moquete, Casper);

Moquete.prototype._start = function(url, comment, status_code) {
    this.start(function() {
        this.open(url);
        this.test.assertHttpStatus(status_code | 200);
        this.test.comment(comment | (url + ' starts!'));
        this.test.info('[moquete] start');
    });
};

Moquete.prototype.thenOpenAndExistXpath = function(url, path, status_code) {
    this.thenOpen(url, function(){
        this.test.assertHttpStatus(status_code | 200);
        this.test.comment('[moquete] Open the page ('+ this.getTitle() + ')');
        this.test.info('[moquete] Current location is ' + this.getCurrentUrl());
        this.test.assertExist(xpath(path), path + ' exists!');
    });
};

Moquete.prototype.thenClickAndWaitFor = function(path, check, then, timeout) {
    this.thenClick(xpath(path), function() {
        this.waitFor(check, then, timeout);
    });
};

Moquete.prototype.thenClickAndWaitForUrl = function(path, url, then, timeout) {
    this.thenClick(xpath(path), function() {
        this.waitForUrl(url, then, timeout);
    });
};

Moquete.prototype.thenClickAndWaitForSelector = function(path, selector, success, fail) {
    this.thenClick(xpath(path), function() {
        this.waitForSelector(xpath(selector), success, fail);
    });
};
Moquete.prototype.thenClickAndWaitWhileSelector = function(path, selector, success, fail) {
    this.thenClick(xpath(path), function() {
        this.waitWhileSelector(xpath(selector), success, fail);
    });
};
Moquete.prototype.thenClickAndWaitWhileVisible = function(path, selector, success, fail) {
    this.thenClick(xpath(path), function() {
        this.waitWhileVisible(xpath(selector), success, fail);
    });
};
Moquete.prototype.thenClickAndWaitUntilVisible = function(path, selector, success, fail) {
    this.thenClick(xpath(path), function() {
        this.waitUntilVisible(xpath(selector), success, fail);
    });
};
Moquete.prototype.thenOpenAndWaitForSelector = function(url, selector, success, fail) {
    this.thenOpen(url, function() {
        this.waitForSelector(xpath(selector), success, fail);
    });
};

Moquete.prototype.thenClickAndWaitForUrlChange = function(path ,then, onTimeout, timeout){
    var oldUrl;
    this.thenClick(xpath(path), function() {
        oldUrl = this.getCurrentUrl();
        this.waitFor(function check(){
             return oldUrl === this.getCurrentUrl();
        }, then, onTimeout, timeout);
    });  
};

Moquete.prototype.thenWaitForSelector = function(path, success, fail) {
    this.then(function() {
    this.waitForSelector(xpath(path), success, fail);
    });
};

Moquete.prototype.thenWaitFor = function(selector, n, then, timeout) {
    this.then(function() {
        this.waitFor(function check() {
            return this.getElementsInfo(xpath(selector)).length == n ;
        }, then, timeout);
    }); 
};


Moquete.prototype._waitForSelector = function(path, success, fail) {
    this.waitForSelector(xpath(path), success, fail);
};
Moquete.prototype.printTitle = function() {
    this.echo('### ' + this.getTitle() + ' ###', 'INFO_BAR');
};

Moquete.prototype.clearCookies = function() {
    this.test.info("Clear cookies");
    this.page.clearCookies();
};

Moquete.prototype.captures_counter = 0;

Moquete.prototype.capturePage = function (debug_name) {
    var directory = 'captures/' + this.test.currentSuite.name;
    if (this.captures_counter > 0) {
        var previous = directory + '/step-' + (this.captures_counter-1) + '.png';
        if (debug_name && this.exists("body")) {
            var current = directory + '/step-' + this.captures_counter + '-' + debug_name + '.png';
        } else {
            var current = directory + '/step-' + this.captures_counter + '.png';
        }
        this.captureSelector(current, 'html');
 
        // If previous is same as current (and no debug_name), remove current
        if (!debug_name && fs.isFile(previous) && fs.read(current) === fs.read(previous) && fs.isFile(current)) {
            fs.remove(current);
            this.captures_counter--;
            this.log('Capture removed because same as previous', 'warning');
        }
    } else {
        // We remove the directory to cleanup
        fs.removeTree(directory);
    }
    this.captures_counter++;
};

Moquete.prototype._run = function () {
    this.run(function() {this.test.done();});
}

Moquete.prototype._get = function (url, headers, then, timeout) {
    var options = {
        method: 'get',
        headers: headers
    };
    this.thenOpen(url, options, then, timeout, responseHandler.bind(this));
}

Moquete.prototype._post = function (url, headers, data, then, timeout) {
    var options = {
        method: 'post',
        headers: headers,
        data: JSON.stringify(data)
    };
    this.thenOpen(url, options, then, timeout, responseHandler.bind(this));
}

Moquete.prototype._put = function (url, headers, data, then, timeout) {
    var options = {
        method: 'put',
        headers: headers,
        data: JSON.stringify(data)
    };
    this.thenOpen(url, options, then, timeout, responseHandler.bind(this));
}

Moquete.prototype._delete = function (url, headers, then, timeout) {
    var options = {
        method: 'delete',
        headers: headers
    };
    this.thenOpen(url, options, then, timeout, responseHandler.bind(this));
}

exports.Moquete = Moquete;


const responseHandler = function(response) {
    this.echo('----status'+ response.status); 
    utils.dump(response);  
    this.echo(this.page.plainText); 
}