//C:\tests>casperjs test login.js
// Configuration and some useful methods
 
/**
 * Debug/Verbose
 * ----------------------------------------------------------------------------
 */
var Moquete = require('./moquete').Moquete;

var moquete = new Moquete();
var debug_mode = !!casper.cli.get('verbose');
if (debug_mode) {
    debug_mode = true;
    moquete.options.verbose = true;
    moquete.options.logLevel = 'debug';
}
var colorizer = require('colorizer').create('Colorizer'); //para que salgan de distintos colores los textos
var mouse = require("mouse").create(moquete);

/**
 * The view
 * ----------------------------------------------------------------------------
 */
 
// The viewport size
moquete.options.viewportSize = {
    width: 1280,
    height: 800
};
moquete.options.waitTimeout =  20000;
/**
 * The HTTP responses
 * ----------------------------------------------------------------------------
 */
moquete.options.httpStatusHandlers = {
    200:  function(self, resource) {
        this.echo(resource.url + " is OK (200)", "INFO");
    },
    400:  function(self, resource) {
        this.echo(resource.url + " is nok (400)", "INFO");
    },
    404: function(self, resource) {
        this.echo("Resource at " + resource.url + " not found (404)", "COMMENT");
    },
    302: function(self, resource) {
        this.echo(resource.url + " has been redirected (301)", "INFO");
    }
};
/**
 * Login credentials
 * ----------------------------------------------------------------------------
 */

var users = {
    test001:{
        emailLogin: 'martapruebas@mailinator.com',
        passwordLogin: 'testeci0',
        cuc: '0129483269'
        /*emailLogin: 'select_shipping@mailinator.com',
        passwordLogin: '123456',
        cuc: '0221252885'*/
    },
    test002:{
        emailLogin: 'test002@mailinator.com',
        passwordLogin: '123456',
        cuc: '0131620270'
    }
};

var endpoints = {
    uat: {
        cart: 'https://promise-food.uat.eci.geci/food/v1.0/carts/:cuc.:shop'
    },
     nft: {
        cart: 'https://promise-food.nft.eci.geci/food/v1.0/carts/:cuc.:shop',
        items: 'https://promise-food.nft.eci.geci/food/v1.0/carts/:cuc.:shop/items/list'
    },
     pro: {
        cart: 'http://192.168.88.36:8080/food/v1.0/carts/:cuc.:shop',
        items: 'http://192.168.88.36:8080/food/v1.0/carts/:cuc.:shop/items/list'
    }
};
/**
 * Product credentials
 * ----------------------------------------------------------------------------
 */
var prod_url_simple = '0110118840100616-pazo-de-vilane-huevo-moreno-de-gallinas-camperas-clase-l-estuche-1-docena/';
var prod_name_simple = 'PAZO DE VILANE huevo moreno de gallinas camperas clase L estuche 1 docena';
//var prod_url_conf ='men/new-arrivals/chelsea-tee.html';
//var prod_name_conf = 'Chelsea Tee';
 
/**
 * URLs
 * ----------------------------------------------------------------------------
 */
//var siteName = 'Autenticaci?n y Registro';
var base_url = 'https://www.nft.elcorteingles.es/supermercado';
if (!/\/$/.test(base_url)) {
    // We have not trailing slash: add it
    base_url = base_url + '/';
}

 // Done for the test file
// ----------------------------------------------------------------------------
casper.test.done();
 
/**
 * Steps
 * ----------------------------------------------------------------------------
 */
 
moquete.on("load.failed", function() {
    moquete.capturePage();
});
 
moquete.on("load.finished", function() {
    moquete.printTitle();   moquete.capturePage();
});
 
moquete.on("fill", function() {
    moquete.capturePage();
});
 
moquete.on("mouse.down", function() {
    moquete.capturePage();
});
 
moquete.on("mouse.move", function() {
    moquete.capturePage();
});
 
moquete.on("mouse.move", function() {
    moquete.capturePage();
});
 
moquete.on("step.complete", function() {
    moquete.capturePage();
});
 
moquete.on("step.error", function() {
    moquete.capturePage('error');
});
 
moquete.on("http.status.500", function() {
    moquete.capturePage('500');
});
 
moquete.on("http.status.404", function() {
    moquete.capturePage('404');
});
