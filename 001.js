//casperjs --ignore-ssl-errors=true --pre=config.js test 001.js

var login   = require('actions/login');
var cart   = require('actions/cart');
var xpath   = require('casper').selectXPath;

moquete.test.begin('001', function suite() {
    moquete._start('base_url', '001 open supermarket');
    moquete.thenOpenAndExistXpath(base_url, '//span[text()="Alimentación"]');

    login.food_login();
    cart.search_and_add('platano');
    moquete._run();
});