//casperjs --ignore-ssl-errors=true --pre=config.js test shopping.js

const login   = require('actions/login');
const xpath   = require('casper').selectXPath;

moquete.test.begin('shopping', function suite() {
    moquete._start('base_url', '[moquete] Open Supermarket Tab');
    moquete.thenOpenAndExistXpath(base_url, '//span[text()="Alimentación"]');
    
    moquete.thenClickAndWaitForUrl('//span[text()="Alimentación"]', base_url + 'alimentacion-general', function() {
        this.test.info('Current location is ' + this.getCurrentUrl());
        this.test.assertHttpStatus(200);
        this.test.assertExists(xpath('//ul[@class="tree-node"]'), 'arbol de navegacion');
    });

    moquete.thenClickAndWaitForUrl('//a[text()="Arroz"]', base_url + 'alimentacion-general/arroz/',
        function() {
            this.test.assertExists(xpath('//button[@class="product_controls-button _buy button _primary _not_perishable js-add-item js-tooltip"]'), 'existen productos para añadir');
            this.echo('---------------------------------------');
            this.echo(this.getElementsInfo(xpath('//button[@class="product_controls-button _buy button _primary _not_perishable js-add-item js-tooltip"]')).length);
        },
        function() {
            this.echo('Fallo al ir a arroz', 'ERROR');
        }
    );
    moquete.then( function(){
        this.clickLabel('añadir al carro', 'span');
        this.wait(4000);

    });

    moquete._waitForSelector('//a[text()="Iniciar sesión"]',
        function () {
            this.test.assertExists({type: 'xpath', path: '//*[@id="modal-content"]/div/div/div[3]/a[1]'},'Existe la modal para iniciar sesión');
            this.test.assertExists({type: 'xpath', path: '//a[text()="Iniciar sesión"]'}, 'Existe el botón de iniciar sesión');
        },
        function () {
            this.echo('Fallo al clickar en iniciar sesión', 'ERROR'); 
         }
    );
    moquete.thenClickAndWaitForUrl('//a[text()="Iniciar sesión"]', /oauth\/authorize/,
        function () {
            this.test.assertExists({type: 'xpath', path: '//*[@type="submit"]'}, 'Existe el botón de Acceder');
        },
        function () {
            this.echo('Fallo al clickar en iniciar sesión', 'ERROR'); 
        }
    );
    login.food_modal_login();
 
    moquete.thenClickAndWaitForSelector ('//div[@class="minicart-header js-show-minicart"]', '//a[@class="button _primary minicart-buy_cart js-go-to-full-cart"]', 
        function() {
            this.test.assertExists(xpath('//a[@class="button _primary minicart-buy_cart js-go-to-full-cart"]'), 'Existe el botón al carro');
        },
        function() {
            this.echo('No existe la cesta flotante', 'ERROR');
        }
    );
   
    moquete.thenClickAndWaitForUrl ('//a[@class="button _primary minicart-buy_cart js-go-to-full-cart"]', base_url + 'alimentacion/carro/',
        function (){
            this.test.assertExists(xpath('//div[@class="button _primary js-process-cart js-prepare-cart"]'), 'Existe el botón tramitar');
        },
        function () {
        this.echo('Fallo al ir al carro', 'ERROR');
        });
   
    moquete.thenClickAndWaitForSelector('//div[@class="button _primary js-process-cart js-prepare-cart"]','//button[@class="btn submit js-submit-shipping"]',
        function (){

            this.echo(this.getElementsInfo(xpath('//*[@id="modalSlot"]/div/div/div/div[2]/div[2]/div/div/div[contains(@class,"_OPEN")]')).length);
            this.test.assertExists(xpath('//*[@id="modalSlot"]'), 'Existe la tabla de slots');
        },
        function () {
        this.echo('Fallo al ir al checkout', 'ERROR');
        });
   
    moquete.thenClickAndWaitForSelector('//input[@class="js-slot-input"][contains(@value,"OPEN")]', '//*[@id="js-slot-info"]/p',
        function (){
            this.test.assertExists(xpath('//*[@id="js-clock-container"]/span[3]/span'), 'Existe el contador del tiempo');
        },
        function () {
            this.echo('Fallo al reservar tramo', 'ERROR');
        });

    moquete.thenClickAndWaitForSelector('//*[@id="unfolded-cart-summary-container"]/div[4]/button', '//*[@id="RECEIVABLE"]/a',
        function (){
            this.test.pass("Existe el botón contrareembolso")
            //this.test.assertExists({type: 'xpath', path: '//*[@id="js-clock-container"]/span[3]/span' }, 'Existe el contador del tiempo');
        },
        function () {
            if(this.exists(xpath('//button[@class="js-message-accept button _primary"]'))){
                this.thenClick(xpath('//button[@class="js-message-accept button _primary"]'));
            }else{
            this.echo('Fallo al ir a la PGP', 'ERROR');
            }
        });
   
    moquete.thenClickAndWaitForSelector('//*[@id="RECEIVABLE"]/a', '//*[@id="PAGAR_COMPRA"]',
        function (){
            this.test.pass("Existe el botón de pagar compra")
            //this.test.assertExists({type: 'xpath', path: '//*[@id="js-clock-container"]/span[3]/span' }, 'Existe el contador del tiempo');
        },
        function () {
            this.echo('Fallo al pulsar contrareembolso', 'ERROR');
        });
   
    moquete.thenClickAndWaitForSelector('//*[@id="PAGAR_COMPRA"]', '//div[@class="c12 confirmation"]',
     function (){
            this.test.assertExists(xpath('//p[text()="Tu número de pedido es "]'), 'Existe el número de pedido');
            this.test.assertExists(xpath('//span[@class="time-slot"]'), 'Existe la fecha de entrega');
            this.test.assertExists(xpath('//p[text()="Método de pago: "]'), 'Existe forma de pago');
            this.test.assertExists(xpath('//a[@class="button _primary cart_control"]'), 'Existe el botón continuar');
            this.test.assertExists(xpath('//div[@class="c12 product_table-description mb"]'), 'Existe el botón continuar');
           // var products=require('utils').dump(this.getElementsAttribute('//div[@class="c12 product_table-description mb"]', 'class'));
            this.echo(this.getElementsInfo(xpath('//div[@class="c12 product_table-description mb"]')).length);
            
        },
        function () {
            this.echo('Fallo al ir a confirmación', 'ERROR');
        });

    moquete.run(function () {
            this.test.done();
        }
    )
});