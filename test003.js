//casperjs --ignore-ssl-errors=true --pre=config.js test test003.js
/*
**Test para probar una compra completa contra reembolso, centro GENIL, entrada usuario logado, crear carro desde mis listas.
*/
var require = patchRequire(require);
const login   = require('actions/login');
const cart   = require('actions/cart');
const xpath   = require('casper').selectXPath;
var f_s = require('../selectors/food').selectors;
var mya_s = require('../selectors/my_account').selectors;

var countFunction = function(count) {
  moquete.thenClickAndWaitForSelector (f_s.plp.add_no_in_cart_s, '('+ f_s.plp.in_cart_s +')['+ count +']',
    function success() {
      this.test.assertEquals(this.getElementsInfo(xpath(f_s.plp.in_cart_s)).length,count, 'Hay '+ count +' elementos en el carro');
    },
    function fail() {
      this.echo(this.getElementsInfo(xpath(f_s.plp.in_cart_s)).length);
      this.echo(count);
      this.echo('Fallo al añadir el producto');
  });
};


moquete.test.begin('Test003', function(test) {

  moquete._start();

cart.delete(endpoints.nft.cart, users.test001.cuc,'01',
      function then() {
        this.test.assertHttpStatus(204);
      },
    function timeout() {
    this.echo('Fallo al hacer el delete');
  });

	moquete.thenOpenAndWaitForSelector(base_url, f_s.modals.cookies,
      function success(){
        this.test.assertExists(xpath(f_s.modals.cookies), 'Existe la modal de cookies');
      },
      function fail(){
        this.echo("No existe la modal de cookies");
   });

   	moquete.thenClickAndWaitWhileVisible(f_s.modals.cookies, f_s.modals.cookies,
    	function success(){
       		this.test.assertDoesntExist(xpath(f_s.modals.cookies), "No existe la modal de cookies");
      	},
    	function fail(){
        	this.echo("No existe la modal de cookies");
   	});

    moquete.thenClickAndWaitForUrl(f_s.login.sign_in, /oauth\/authorize/,
        function success() {
            this.test.assertExists(xpath(mya_s.login.submit), 'Existe el botón de Acceder');
        },
        function fail() {
            this.echo('Fallo al clickar en iniciar sesión', 'ERROR'); 
        });
    login.food_login(users.test001.emailLogin,users.test001.passwordLogin, 
      function callback(){
         this.test.assertExists(xpath(f_s.login.logged), 'Se ha logado el usuario');
      });

    moquete.thenClickAndWaitForSelector (f_s.user_controls.my_lists, f_s.my_lists.modal_new_user,
      function success() {
          this.test.assertExists(xpath(f_s.my_lists.modal_new_user), 'Existe la modal de ayuda de lista');
      },
      function fail() {
          this.echo('No existe el botón de mis listas');
  });
	moquete.thenClickAndWaitWhileVisible (f_s.my_lists.modal_new_user, f_s.my_lists.modal_new_user,
	    function success() {
	        this.test.assertDoesntExist(xpath(f_s.my_lists.modal_new_user), 'No existe la modal de ayuda de lista');
	        this.test.assertExists(xpath(f_s.my_lists.name_new_list), 'Existe el campo para introducir nueva lista');
          this.echo("Número listas: " + this.getElementsInfo(xpath(f_s.my_lists.list)).length);
	    },
	    function fail() {
	        this.echo('No existe la modal de ayuda');
	});

	 moquete.thenClickAndWaitForSelector('//*[@id="gl066757"]', f_s.modals.temporal_order.change,
        function success() {
            this.test.assertExist(xpath(f_s.modals.temporal_order.change), 'Existe la modal de carro temporal');
        },
        function fail() {
            this.echo('Fallo al clickar la lista');
    });

    moquete.thenClickAndWaitForSelector(f_s.modals.temporal_order.change, f_s.select_shipping.choose_centre,
        function success() {
                  this.test.assertExists(xpath(f_s.select_shipping.choose_centre), 'Existe seleccionar centro');
            this.test.assertExists(xpath(f_s.select_shipping.choose_address), 'Existe seleccionar dirección');
        },
        function fail() {
            this.echo('Fallo al ir a la página de seleccionar metodo de envio');
        }
    );
   moquete.thenClickAndWaitForSelector (f_s.select_shipping.choose_centre, f_s.select_centre.droppable,
        function success() {
            this.test.assertExists(xpath(f_s.select_centre.droppable), 'Existe el desplegable de centros');
        },
        function fail() {
            this.echo('Fallo al ir a elegir centro');
    });
    moquete.thenClickAndWaitForSelector (f_s.select_centre.droppable, f_s.select_centre.granada,
        function success() {
            this.test.assertExists(xpath(f_s.select_centre.granada), 'Existe Granada');
        },
        function fail() {
            this.echo('Fallo al clickar el desplegable');
    });

     moquete.thenClickAndWaitForSelector (f_s.select_centre.granada, f_s.select_centre.genil,
        function success() {
            this.test.assertExists(xpath(f_s.select_centre.genil), 'Existe El centro Genil' );
        },
        function fail() {
            this.echo('Fallo al clickar en Granada');
    });
     moquete.thenClickAndWaitForSelector (f_s.select_centre.genil, f_s.detailed_list.add_cart,
        function success() {
            this.test.assertExists(xpath(f_s.detailed_list.add_car), 'Existe añadir a la cesta' );
        },
        function fail() {
            this.echo('Fallo al seleccionar genil');
    });
    moquete.thenClickAndWaitForSelector (f_s.detailed_list.add_cart, f_s.mini_cart.show,
        function success() {
            this.test.assertExists(xpath(f_s.mini_cart.show), 'Existe el minicarro' );
        },
        function fail() {
            this.echo('Fallo al añadir a la lista');
    });
  moquete.thenClickAndWaitForSelector(f_s.mini_cart.show, f_s.mini_cart.full_cart,
        function success() {
            this.test.assertExist(xpath(f_s.mini_cart.full_cart), 'Existe el botón ir a la página de carro');
        },
        function fail() {
            this.echo('Fallo al abrir el minicarro');
    });
    moquete.thenClickAndWaitForSelector(f_s.mini_cart.full_cart, f_s.cart.to_checkout,
        function success() {
            this.test.assertExist(xpath(f_s.cart.to_checkout), 'Existe el botón para ir al checkout');
            this.test.assertExist(xpath(f_s.cart.delete_cart), 'Existe el botón borrar el carro');
            this.test.assertExist(xpath(f_s.cart.delete_item), 'Existe el botón para borrar items');
            this.test.assertExist(xpath(f_s.cart.save_as_list), 'Existe el botón para guardar una lista');
        },
        function fail() {
            this.echo('Fallo al ir a la página de carro');
    });
 	moquete.thenClickAndWaitForSelector(f_s.cart.to_checkout, f_s.checkout.to_pgp,
        function success() {
          if(this.exists(xpath(f_s.checkout.to_pgp))){
            this.test.assertExist(xpath(f_s.checkout.to_pgp), 'Existe el botón para ir a la PGP');
            this.echo(this.getElementsInfo(xpath(f_s.checkout.open_slot)).length);
            this.test.assertExists(xpath(f_s.checkout.slots_table), 'Existe la tabla de slots');
         }else{
            this.thenClickAndWaitWhileSelector(f_s.loyalty.to_checkout, f_s.checkout.to_pgp)
         }
        },
        function fail() {
            this.echo('Fallo al ir al checkout');
    });
    moquete.thenClickAndWaitForSelector(f_s.checkout.open_slot, f_s.checkout.clock,
        function success() {
            this.test.assertExist(xpath(f_s.checkout.clock), 'Existe el contador de tiempo');
            this.test.assertExist(xpath(f_s.checkout.slot_info), 'Existe la información del tramo seleccionado');
        },
        function fail() {
            this.echo('Fallo al seleccionar el tramo');
    });
   /*
      moquete._waitForSelector (f_s.checkout.eci_card_number, 
        function success() {
          this.test.assertExists(xpath(f_s.checkout.eci_card_number));
          this.sendKeys('#card_number', "0079052684");
          this.sendKeys('#pin_number', "018");
        },
        function fail() {
            this.echo('No existen los campos para teci');
        });
       moquete._waitForSelector (f_s.checkout.eci_card_yn, 
        function success() {
          this.test.assertExists(xpath(f_s.checkout.eci_card_yn));
          this.test.assertExists(xpath(f_s.checkout.eci_card_discounts));
        },
        function fail() {
            this.echo('No se ha introducido la teci');
        });
  
 	  moquete.thenClickAndWaitForSelector(f_s.checkout.to_pgp, f_s.pgp.eci_card,
        function success() {
            this.test.assertExist(xpath(f_s.pgp.eci_card), 'Existe el pago con TECI');
        },
        function fail() {
            this.echo('Fallo al continuar a PGP');
    });
    moquete.thenClickAndWaitForSelector(f_s.pgp.eci_card, f_s.pgp.eci_card_doc_id,
        function success() {
            this.test.assertExist(xpath(f_s.pgp.eci_card_doc_id), 'Existe el campo para introducir el documento');
        },
        function fail() {
            this.echo('Fallo al pulsar teci');
    });
    moquete._waitForSelector (f_s.pgp.eci_card_doc_id, 
       function success() {
        this.test.assertExists(xpath(f_s.pgp.eci_card_doc_id));
        this.test.assertExists(xpath(f_s.pgp.eci_card_pc));
        this.sendKeys('#numDocumento', "46888594N");
        this.sendKeys('#codPostal', "05003");
      },
        function fail() {
            this.echo('No existen los campos para teci');
        });
        */

      moquete.thenClickAndWaitForSelector(f_s.checkout.to_pgp, f_s.pgp.contrarrembolso,
        function success() {
            this.test.assertExist(xpath(f_s.pgp.contrarrembolso), 'Existe el pago con contrarrembolso');
        },
        function fail() {
            this.echo('Fallo al continuar a PGP');
    });
    moquete.thenClickAndWaitForSelector(f_s.pgp.contrarrembolso, f_s.pgp.pay,
        function success() {
            this.test.assertExist(xpath(f_s.pgp.pay), 'Existe el botón para pagar');
        },
        function fail() {
            this.echo('Fallo al pulsar contrarrembolso');
    });
      moquete.thenClickAndWaitForUrl(f_s.pgp.pay, /confirmacion/,
      function success(){
          this.test.assertExists(xpath(f_s.confirmation), 'Existe la página de confirmacion');
        },
      function fail(){
          this.echo("Fallo al pagar en PGP")
    });

    moquete.run(function() {test.done(); this.exit()});
});