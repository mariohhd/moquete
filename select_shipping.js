//casperjs --ignore-ssl-errors=true --pre=config.js test select_shipping.js
var require = patchRequire(require);
const login   = require('actions/login');
const x   = require('casper').selectXPath;


moquete.test.begin('Resurrectio test', function(test) {

  moquete.start();

   moquete.delete('http://192.168.88.36:8080/food/v1.0/carts','0221252885','01',
   function then() {
     this.thenOpen('https://www.nft.elcorteingles.es/supermercado/');
   },
  function timeout() {
       this.echo('Fallo en la peticion');
 
   moquete.waitForSelector("form#desktopSearchForm input[name='term']",
       function success() {
           test.assertExists("form#desktopSearchForm input[name='term']");
           this.click("form#desktopSearchForm input[name='term']");
       },
       function fail() {
           test.assertExists("form#desktopSearchForm input[name='term']");
   });
   moquete.waitForSelector("input[name='term']",
       function success() {
           this.sendKeys("input[name='term']", "platanos");
       },
       function fail() {
           test.assertExists("input[name='term']");
       
   });
   moquete.waitForSelector(x("//*[@id='panel_products']/div[2]/div[1]/div/div[4]/div/div/button[2]"),
        function success() {
            this.click(x('//button[@class="button quantity_control-button _mini js-stepper-button"][@data-stepper-direction="up"]'));
        },
        function fail() {
            test.assertExists("input[name='term']");
        
    });
   moquete.waitForSelector(x("//a[normalize-space(text())='Iniciar sesión']"),
       function success() {
           test.assertExists(x("//a[normalize-space(text())='Iniciar sesión']"));
           this.click(x("//a[normalize-space(text())='Iniciar sesión']"));
       },
       function fail() {
           test.assertExists(x("//a[normalize-space(text())='Iniciar sesión']"));
   });
   moquete.waitForSelector("form input[name='login']",
       function success() {
           test.assertExists("form input[name='login']");
           this.click("form input[name='login']");
       },
       function fail() {
           test.assertExists("form input[name='login']");
   });
   moquete.waitForSelector("input[name='login']",
       function success() {
           this.sendKeys("input[name='login']", "select_shipping@mailinator.com");
       },
       function fail() {
           test.assertExists("input[name='login']");
   });
   moquete.waitForSelector("input[name='password']",
       function success() {
           this.sendKeys("input[name='password']", "123456");
       },
       function fail() {
           test.assertExists("input[name='password']");
   });
   moquete.waitForSelector("form input[type=submit][value='Acceder']",
       function success() {
           test.assertExists("form input[type=submit][value='Acceder']");
           this.click("form input[type=submit][value='Acceder']");
       },
       function fail() {
           test.assertExists("form input[type=submit][value='Acceder']");
   });
   /* submit form */
   moquete.waitForSelector(x('//*[@id="modal-content"]/div/div[3]/a'),
       function success() {
           test.assertExists(x('//*[@id="modal-content"]/div/div[3]/a'));
           this.click(x('//*[@id="modal-content"]/div/div[3]/a'));
       },
       function fail() {
           //test.assertExists(x("//a[normalize-space(text())='INICIAR PEDIDO']"));
           moquete.open('https://www.elcorteingles.es/alimentacion/metodo-de-envio/')
   });
   moquete.waitForSelector(".c12.mb.c6-m-up.pr-m-up .area.p2",
       function success() {
           test.assertExists(".c12.mb.c6-m-up.pr-m-up .area.p2");
           this.click(".c12.mb.c6-m-up.pr-m-up .area.p2");
       },
       function fail() {
           test.assertExists(".c12.mb.c6-m-up.pr-m-up .area.p2");
   });
   moquete.waitForSelector(x('//button[@class="droppable-button _provinces js-droppable-button"][@type="button"]'),
       function success() {
           test.assertExists(x('//button[@class="droppable-button _provinces js-droppable-button"][@type="button"]'));
           this.click(x('//button[@class="droppable-button _provinces js-droppable-button"][@type="button"]'));
       },
       function fail() {
           test.assertExists(".droppable-button._provinces.js-droppable-button");
   });
   moquete.waitForSelector(x('//li[@class="droppable-item _province js-droppable-option"][@data-droppable-value="28"]'),
       function success() {
           test.assertExists(x('//li[@class="droppable-item _province js-droppable-option"][@data-droppable-value="28"]'));
           this.click(x('//li[@class="droppable-item _province js-droppable-option"][@data-droppable-value="28"]'));
       },
       function fail() {
           test.assertExists(x('//li[@class="droppable-item _province js-droppable-option"][@data-droppable-value="28"]'));
   });

   moquete.waitForSelector('body > div.page.c12 > div > div.page-vp > div.c12.js-select-center-page > div.c12.js-select-center > div > div.c12.c6-l-up.c5-xxl-up.p1-l-up.p2-xxl-up > div.scroll_list-container._centers.c12 > ul > li:nth-child(3) > div:nth-child(2) > div:nth-child(2) > button',
        function success() {
            test.assertExists('body > div.page.c12 > div > div.page-vp > div.c12.js-select-center-page > div.c12.js-select-center > div > div.c12.c6-l-up.c5-xxl-up.p1-l-up.p2-xxl-up > div.scroll_list-container._centers.c12 > ul > li:nth-child(3) > div:nth-child(2) > div:nth-child(2) > button');
           this.click('body > div.page.c12 > div > div.page-vp > div.c12.js-select-center-page > div.c12.js-select-center > div > div.c12.c6-l-up.c5-xxl-up.p1-l-up.p2-xxl-up > div.scroll_list-container._centers.c12 > ul > li:nth-child(3) > div:nth-child(2) > div:nth-child(2) > button');
        },
        function fail() {
            test.assertExists(x('//button[@class="button _primary js-center-select"]'));
    });
   moquete.waitForSelector(x('//*[@id="modal-content"]/div/div/div[3]/button'),
       function success() {
           test.assertExists(x('//*[@id="modal-content"]/div/div/div[3]/button'));
           this.click(x('//*[@id="modal-content"]/div/div/div[3]/button'));
       },
       function fail() {
           test.assertExists(x('//*[@id="modal-content"]/div/div[2]/div[3]/div[4]/div[2]/a'));
   });

   /*moquete.waitForSelector(x('//*[@id="modal-content"]/div/div[2]/div[3]/div[4]/div[2]/a'),
       function success() {
           test.assertExists(x('//*[@id="modal-content"]/div/div[2]/div[3]/div[4]/div[2]/a'));
           this.click(x('//*[@id="modal-content"]/div/div[2]/div[3]/div[4]/div[2]/a'));
       },
       function fail() {
           test.assertExists(x("//a[normalize-space(text())='COMENZAR A COMPRAR']"));
   });*/
   moquete.waitForSelector("form#desktopSearchForm input[name='term']",
       function success() {
           test.assertExists("form#desktopSearchForm input[name='term']");
           this.click("form#desktopSearchForm input[name='term']");
       },
       function fail() {
           test.assertExists("form#desktopSearchForm input[name='term']");
   });
   moquete.waitForSelector("input[name='term']",
       function success() {
           this.sendKeys("input[name='term']", "platano");
       },
       function fail() {
           test.assertExists("input[name='term']");
   });

  moquete.waitForSelector(x("//*[@id='panel_products']/div[2]/div[1]/div/div[4]/div/div/button[2]"),
      function success() {
        this.click(x('//button[@class="button quantity_control-button _mini js-stepper-button"][@data-stepper-direction="up"]'));
      },
     function fail() {
        test.assertExists("input[name='term']"); 
    });
   moquete.waitForSelector(x('//div[@class="minicart-header js-show-minicart"]'),
       function success() {
           test.assertExists(x('//div[@class="minicart-header js-show-minicart"]'));
           this.click(x('//div[@class="minicart-header js-show-minicart"]'));
       },
       function fail() {
           test.assertExists(x('//div[@class="minicart-header js-show-minicart"]'));
   });
   moquete.waitForSelector(x('//a[@class="button _primary minicart-buy_cart js-go-to-full-cart"]'),
       function success() {
           test.assertExists(x('//a[@class="button _primary minicart-buy_cart js-go-to-full-cart"]'));
           this.click(x('//a[@class="button _primary minicart-buy_cart js-go-to-full-cart"]'));
       },
       function fail() {
           test.assertExists(x('//a[@class="button _primary minicart-buy_cart js-go-to-full-cart"]'));
   });
    moquete.waitForSelector(x('//a[@class="button _primary js-process-cart"]'),
       function success() {
           test.assertExists(x('//a[@class="button _primary js-process-cart"]'));
           this.click(x('//a[@class="button _primary js-process-cart"]'));
       },
       function fail() {
           test.assertExists(x('//a[@class="button _primary js-process-cart"]'));
   });

   moquete.run(function() {test.done();});
});