var require = patchRequire(require);
var s = require('../selectors/food').selectors;
const xpath   = require('casper').selectXPath;


exports.search_and_add = function(t) {

   moquete.waitForSelector(s.searching.input, function() {
        this.sendKeys(s.searching.input, t);
    }),
    function() {
        this.echo('Fallo searching input', 'ERROR');
    };

    moquete._waitForSelector(s.searching.products, function() {
        this.click(xpath(s.searching.units_up));
    }),
    function() {
        this.echo('Fallo searching products', 'ERROR');
    };
}

exports.get = function (url, userId, shopId, then, timeout) {
    var path = url.replace(':cuc', userId).replace(':shop', shopId);
    moquete._get(path, getCartHeaders(userId), then, timeout);
}

exports.post = function (url, userId, shopId, body, then, timeout) {
    var path = url.replace(':cuc', userId).replace(':shop', shopId);
    moquete._post(path, getCartHeaders(userId), body, then, timeout);
}

exports.put = function (url, userId, shopId, body, then, timeout) {
    var path = url.replace(':cuc', userId).replace(':shop', shopId);
    moquete._put(path, getCartHeaders(userId), body, then, timeout);
}

exports.delete = function (url, userId, shopId, then, timeout) {
    var path = url.replace(':cuc', userId).replace(':shop', shopId);
    moquete._delete(path, getCartHeaders(userId), then, timeout);
}


const getCartHeaders = function(userId) {
    return {
        'cache-control': 'no-cache',
        'accept-language': 'es_ES',
        'content-type': 'application/json',
        'x-owner': userId,
        'Origin':'chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop'
    }
}
