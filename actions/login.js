var require = patchRequire(require);
var xpath   = require('casper').selectXPath;
var f_s = require('../selectors/food').selectors;
var mya_s = require('../selectors/my_account').selectors;


exports.food_login = function(email, password, callback) {
 
    moquete.then(function() {
        this.test.comment('Fill and submit login form');
        var o = {};
        o[mya_s.login.login] = email;
        o[mya_s.login.password] = password;
        this.fillXPath(mya_s.login.form, o, true);
            
        this.thenClick(xpath(mya_s.login.submit), callback);
    });
}