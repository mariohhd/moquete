var require = patchRequire(require);

exports.selectors = {
    login: {
        form:      'form[class="c12 js-checkeable-form"]',
        login:     '//input[@name="login"]',
        password:  '//input[@name="password"]',
        submit:    '//*[@type="submit"]'
    }
}