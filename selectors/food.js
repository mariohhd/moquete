var require = patchRequire(require);

exports.selectors = {
    supermarket_tabs:{
        food:'//a[@class="top_menu-item"]',
        breakfast:'(//a[@class="top_menu-item"])[2]',
        dairy:'(//a[@class="top_menu-item"])[3]',
        frozen:'(//a[@class="top_menu-item"])[4]',
        diet:'(//a[@class="top_menu-item"])[5]',
        drinks:'(//a[@class="top_menu-item"])[6]',
        fresh:'(//a[@class="top_menu-item"])[7]',
        baby:'(//a[@class="top_menu-item"])[8]',
        personal:'(//a[@class="top_menu-item"])[9]',
        chemist:'(//a[@class="top_menu-item"])[10]'
    },
    categories: {
        tree: '//ul[@class="tree-node"]',
        family: '//a[@class="link _primary _level3 tree-link  "]',
        variety: '//a[@class="link _primary _level4 tree-link  "]' 
    },
    searching: {
        input:     '//div[@class="search-input-container"]',
        panel_products:  '//*[@id="panel_products"]/div[2]/div[1]/div/div[4]/div/div/button[2]',
        product_result: '//a[@class="event quicksearch_item-text js-typeahead-link"]',
        units_up:  '//button[@class="button quantity_control-button _mini js-stepper-button"][@data-stepper-direction="up"]',
        units_down:  '//button[@class="button quantity_control-button _mini js-stepper-button"][@data-stepper-direction="down"]',
        quantity_modal: '//button[@class="button quicksearch_item-set_quantity js-popup-trigger"]',
        search_button: '//button[@class="button search-button"]'
    },
    login: {
        link:   '//a[@class="link user_bar-item js-custom-gtm-event"]',
        logged: '//span[@class="text cross_site_bar-name"]',
        sign_in: '//a[contains(@class,"cross_site_bar-item")]'
        
    },
    start_order: {
    	from_modal: '//a[@class="button _primary js-start-order-proposal js-custom-gtm-event"]',
    	from_mini_cart: '//div[@class="minicart-header _no-order"]',
    	from_new_user_modal: '//a[@class="button _primary js-custom-gtm-event"]'
    },
    select_shipping: {
    	choose_centre: '//div[@class="deliver_button-hot_zone text _xs _brand2 _uppercase tc"]',
    	choose_address: '(//div[@class="deliver_button-hot_zone text _xs _brand2 _uppercase tc"])[2]'
    },
    select_centre:{
    	droppable: '//button[@class="droppable-button _provinces js-droppable-button"]',
    	madrid: '//li[@class="droppable-item _province js-droppable-option"][@data-droppable-value="28"]',
    	barcelona: '//li[@class="droppable-item _province js-droppable-option"][@data-droppable-value="08"]',
        granada: '//li[@class="droppable-item _province js-droppable-option"][@data-droppable-value="18"]',
    	gasolinera: '//li[@class="c12 scroll_list-item js-center-item delivery_place"][contains(@data-center,"010973")]/div[2]/div[2]/button',
    	plaza_catalunya: '//li[@class="c12 scroll_list-item js-center-item delivery_place"][contains(@data-center,"010002")]/div[2]/div[2]/button',
        genil: '//li[@class="c12 scroll_list-item js-center-item delivery_place"][contains(@data-center,"010915")]/div[2]/div[2]/button'
    },
    user_controls: {
    	delivery_place: '//a[@class="link user_bar-item user_bar-container js-set-general-back-url js-custom-gtm-event"]',
    	panel: '//div[@class="user_controls-controls"]',
    	my_orders: '//a[@class="button button user_controls-control js-custom-gtm-event"][@data-event_name="click_my_orders"]',
    	my_lists: '//a[@class="button button user_controls-control js-custom-gtm-event"][@data-event_name="click_my_list"]',
    	quick_list: '//a[@class="button button user_controls-control js-custom-gtm-event"][@data-event_name="click_quick_list"]',
    	my_usuals: '//a[@class="button button user_controls-control js-custom-gtm-event"][@data-event_name="click_frequent_products"]',
    },
    quick_list:{
    	modal_new_user: '//button[@class="button _primary js-message-accept"]',
    	term: '//input[@class="form-input _big _custom_placeholder js-shopping-list-input"]',
    	add: '//button[@class="button _terciary _plus js-shopping-list-item-submit"]',
    	start: '//button[contains(@class,"start-shopping")]',
    	delete: '//button[@class="button _secondary  js-delete-shopping-list ml"]',
    	list: '//div[@class="tags-tag _term  js-quicklist-tag"]',
    	home_panel: '//div[@class="shopping_list-list tags-container js-shopping-list-list"]',
    	current_term: '//div[@class="shopping_list-current"]',
    	prev_term: '//a[@class="shopping_list-prev"]',
    	next_term: '//a[@class="shopping_list-next"]'
    },
     my_lists: {
        modal_new_user: '//button[@class="button _primary js-message-accept"]',
        create: '//button[@class="button _primary js-create-list"]',
        name_new_list: '//input[@class="form-input form-text _createlist _custom_placeholder _no_margin js-new-list-input"]',
        list: '//div[@class="dataholder list_tile"]',
        delete: '//button[@class="list_tile-icon button js-delete-list"]'
    },
    detailed_list: {
        add_cart: '//button[@class="button _primary js-add-selectable-list-to-cart"]',
        delete_list: '//button[@class="circled_button js-delete-selectable-list"]'

    },
    plp:{
    	units: '//div[@class="quantity_control-quantity product_controls js-quantity js-stepper-input"]',
    	units_up:  '//button[@class="button quantity_control-button product_controls js-stepper-button"][@data-stepper-direction="up"]',
        units_down:  '//button[@class="button quantity_control-button product_controls js-stepper-button"][@data-stepper-direction="down"]',
        in_cart_s: '//div[contains(@class,"_incart")][not(contains(@class,"_advanced"))]/div[2]/div[4]/div/div/button[@class="product_controls-button _buy button _primary _not_perishable js-add-item js-tooltip"]',
        in_cart_c: '//div[contains(@class,"_incart")][contains(@class,"_advanced")]/div[2]/div[4]/div/div/button[@class="product_controls-button _buy button _primary _perishable  js-popup-trigger"]',
        add_no_in_cart_s: '//div[not(contains(@class,"_incart"))][not(contains(@class,"_advanced"))]/div[2]/div[4]/div/div/button[@class="product_controls-button _buy button _primary _not_perishable js-add-item js-tooltip"]',
        add_no_in_cart_c: '//div[not(contains(@class,"_incart"))][contains(@class,"_advanced")]/div[2]/div[4]/div/div/button[@class="product_controls-button _buy button _primary _perishable  js-popup-trigger"]',
        no_in_cart_s: '//div[not(contains(@class,"_incart"))][not(contains(@class,"_advanced"))]',
        no_in_cart_c: '//div[not(contains(@class,"_incart"))][contains(@class,"_advanced")]'
    
    }, 
    modals: {
        cookies:'//*[@id="cookies-agree"]',
        login: {
            sign_in: '//a[@class="button _secondary js-add-proposed_item mb-xs-up mb0-l-up mr-l-up"]',
            sign_up: '//a[@class="button _primary ml-l-up"]'
        },
        catalonia: {
        	close: '//*[@id="modal-close"]',
        	accept: '//a[@class=button _primary"]'
            },
        change_center: {
        	accept: '//button[@class="js-message-accept button _primary mb ml-l-up mb0-l-up"]',
        	cancel: '//button[@class="js-message-cancel button _secondary mb mr-l-up mb0-l-up"]',
        	close: '//*[@id="modal-close"]'
        },
        product_adjusted: {
        	accept: '//button[@class="js-message-accept button _primary"]',
        	items: '//div[@class="c12 mini_item _adjust"]',
        	quantity_ajusted: '//div[@class="mini_item-quantities_adjusted c12"]'
        },
        min_cAc: {
            accept: '//button[@class="js-message-accept button _primary"]',
            text: '//div[text()="30"]',
            close: '//*[@id="modal-close"]'
        },
        slot_required: {
            accept: '//button[@class="js-message-accept button _primary"]',
            close: '//*[@id="modal-close"]'
        },
        out_of_stock: {
            to_pgp: '//button[@class="js-message-accept button _primary ml"]',
            to_cart: '//button[@class="js-message-cancel button _secondary"]'
        },
        temporal_order: {
            keep: '//button[contains(@class,"js-confirm-order")]',
            change: '//a[contains(@class,"change-temporal-order")]'
        }
    },
    mini_cart: {
        show: '//div[@class="minicart-header js-show-minicart"]',
        units: '//div[@class="quantity_control-quantity _mini js-quantity js-stepper-input"]',
        units_up:  '//button[@class="button quantity_control-button _mini js-stepper-button"][@data-stepper-direction="up"]',
        units_down:  '//button[@class="button quantity_control-button _mini js-stepper-button"][@data-stepper-direction="down"]',
        delete_item:  '//button[@class="button mini_item-delete-container p2 js-delete-item"]',
        discounts: '//button[@class="button totals-discount js-open-promotions-modal"]',
        delivery_place: '//a[@class="minicart-order_option minicart-order_info js-set-general-back-url js-custom-gtm-event"]',
        slots: '//div[@class="minicart-order_option minicart-slot_info js-slot-opener"]',
        full_cart: '//a[@class="button _primary minicart-buy_cart js-go-to-full-cart"]',
        delete_cart: '//button[@class="js-delete-cart button minicart-delete_cart"]',
        quantity_modal: '//button[@class="link _primary _underlined js-popup-trigger"]'
    },
    cart: {
        to_checkout:'//div[@class="button _primary js-process-cart js-prepare-cart"]',
        delete_item: '//div[@class="_right_container js-delete-item"]',
        delete_cart: '//button[@class="circled_button js-delete-cart"]',
        save_as_list: '//button[@class="circled_button js-add-holder-to-list"]',
        item: '//div[@class="flex-table-item_container _cart dataholder js-product"]',
        notification: '//div[@class="push_notification-container"]'
    },
    checkout: {
        open_slot: '//input[@class="js-slot-input"][contains(@value,"OPEN")]',
        selected_slot: '//input[@class="js-slot-input"][contains(@value,"selected")]',
        slot_info:'//*[@id="js-slot-info"]/p',
        clock: '//*[@id="js-clock-container"]/span[3]/span',
        slots_table: '//*[@id="modalSlot"]',
        coupons: '//input[@class="c8 coupons-input js-coupon-input"]',
        centre: '//div[@class="c12 js-selected-store selected-store"]',
        eci_card_number: '//*[@id="card_number"]',
        eci_card_pin: '//*[@id="pin_number"]',
        eci_card_discounts: '//*[@id="js-eci-card-discounts"]',
        eci_card_yn: '//*[@id="js-yes-no-inputs"]',
        to_pgp: '//button[@class="btn submit js-submit-shipping"]',
        phone_number: '//*[@id="phone_number"]',
        phone_sms: '//*[@id="phone_sms"]'
    },
    loyalty: {
        to_checkout: '//a[@class="button _primary"]'
    },
    pgp: {
        paypal: '//*[@id="PAYPAL"]/a',
        contrarrembolso: '//*[@id="RECEIVABLE"]/a',
        credit_card: '//*[@id="REDSYS_CARD"]/a',
        eci_card: '//*[@id="ECI_CARD"]/a',
        eci_card_doc_Id: '//*[@id="numDocumento"]',
        eci_card_pc: '//*[@id="codPostal"]',
        pay: '//*[@id="PAGAR_COMPRA"]'
    },
    paypal: {
        sign_in: '//a[@class="btn full ng-binding"]',
        login:  '//*[@id="email"]',
        next:  '//*[@id="btnNext"]',
        password:  '//*[@id="password"]', 
        submit:  '//*[@id="btnLogin"]',
        pay_now:  '//*[@id="confirmButtonTop"]',
    },

    confirmation: '//div[@class="c12 confirmation"]'

}